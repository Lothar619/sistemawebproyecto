<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiantes extends CI_Controller {

	
 public function index()//cargar login
  {
        
        $this->load->view('vistaInicio');



  }

   public function index2()//cargar inicio
  {
      //  $data['estudiantes']=$this->estudiantes_model->retornarEstudiantes();

        $this->load->view('head');
        $this->load->view('vistaLista');



  }
	public function Notas()//cargar registro
	{
        $data['perfilusuario']=$this->estudiantes_model->retornarAu();

        $this->load->view('head');
        $this->load->view('vistanotas',$data);
    }
    public function agregar()//agregar usuario
  {
    
    
    $this->load->view('head');
    $this->load->view('agregarform');
    //$this->load->view('footer');
  }
  public function agregardb()
  {
    
    $nombres=$_POST['nombres'];
    $ps['nombres']=$nombres;
    $primerApellido=$_POST['primerApellido'];
    $ps['primerApellido']=$primerApellido;
    $segundoApellido=$_POST['segundoApellido'];
    $ps['segundoApellido']=$segundoApellido;
    $nombreNiño=$_POST['nombreNiño'];
    $ps['nombreNiño']=$nombreNiño;
    $cursoNiño=$_POST['cursoNiño'];
    $ps['cursoNiño']=$cursoNiño;
    $login=$_POST['login'];
    $ps['login']=$login;
    $password=$_POST['password'];
    $clave_md5=md5($password);
    $ps['password']=$clave_md5;
    $ocupacion=$_POST['ocupacion'];
    $ps['ocupacion']=$ocupacion;
    //$this->paises_model->agregarPais($data);
   // $ps = array('nombres' =>$this->input->post('nombres'),'primerApellido' =>$this->input->post('primerApellido'),'segundoApellido' =>$this->input->post('segundoApellido'),'nombreNiño' =>$this->input->post('nombreNiño'),'cursoNiño' =>$this->input->post('cursoNiño'),'ocupacion' =>$this->input->post('ocupacion'));
         $this->estudiantes_model->agregarU($ps);
    $this->load->view('head');
    $this->load->view('agregarmensaje',$ps);
   // $this->load->view('footer');

  }
    public function registro()
	{
              
              $this->load->view('head');
               $this->load->view('vistaregistro');     

	}

  public function modificar()
  {
    $idperfilUsuario=$_POST['idperfilUsuario'];
    $data['nombres']=$this->estudiantes_model->recuperarU($idperfilUsuario);
    $this->load->view('head');
    $this->load->view('modificarform',$data);
    //$this->load->view('footer');
  }
  
  public function modificardb()
  {
    $idperfilUsuario=$_POST['idperfilUsuario'];
    
    $ps = array('idperfilUsuario' =>$this->input->post('idperfilUsuario'),'nombres' =>$this->input->post('nombres'),'primerApellido' =>$this->input->post('primerApellido'),'segundoApellido' =>$this->input->post('segundoApellido'),'nombreNiño' =>$this->input->post('nombreNiño'),'cursoNiño' =>$this->input->post('cursoNiño'),'ocupacion' =>$this->input->post('ocupacion'));
    

    $this->estudiantes_model->modificarU($idperfilUsuario,$ps);
    $this->load->view('head');
    $this->load->view('modificarmensaje',$ps);
    //$this->load->view('footer');
  //  redirect('paises/index','refresh');
  }
   
   public function eliminardb()
  {
      


    $idperfilUsuario=$_POST['idperfilUsuario'];
    $nombres=$_POST['nombres'];
    $data['nombres']=$nombres;
    $this->estudiantes_model->eliminarU($idperfilUsuario);
    $this->load->view('head');
    $this->load->view('eliminarmensaje',$data);
    //$this->load->view('footer');
  }

    public function contenido()//ver contenido
  {
        $data['contenido']=$this->estudiantes_model->retornarCo();  
        
              $this->load->view('head');
               $this->load->view('vistacontenido',$data);
       

  }
    public function agregarC()//agregar contenido
  {
    
    
    $this->load->view('head');
    $this->load->view('agregarCform');
    //$this->load->view('footer');
  }
    public function agregarCdb()
  {
    
    $nombreArea=$_POST['nombreArea'];
    $c['nombreArea']=$nombreArea;
    $descripcion=$_POST['descripcion'];
   // $clave_md5=md5($descripcion);
    $c['descripcion']=$descripcion;
    //$this->paises_model->agregarPais($data);
    //$c = array('nombreArea' =>$this->input->post('nombreArea'), 'descripcion' =>$this->input->post('descripcion'));
         $this->estudiantes_model->agregarC($c);
    $this->load->view('head');
    $this->load->view('agregarCmensaje',$c);
   // $this->load->view('footer');

  }
  public function modificarC()
  {
    $idcontenido=$_POST['idcontenido'];
    $data['nombreArea']=$this->estudiantes_model->recuperarC($idcontenido);
    $this->load->view('head');
    $this->load->view('modificarCform',$data);
    //$this->load->view('footer');
  }
   public function modificarCdb()
  {
    $idcontenido=$_POST['idcontenido'];
    $c = array('idcontenido' =>$this->input->post('idcontenido'),'nombreArea' =>$this->input->post('nombreArea'),'descripcion' =>$this->input->post('descripcion'));
    

    $this->estudiantes_model->modificarC($idcontenido,$c);
    $this->load->view('head');
    $this->load->view('modificarCmensaje',$c);
    //$this->load->view('footer');
  //  redirect('paises/index','refresh');
  }

  public function eliminarCdb()
  {
      


    $idcontenido=$_POST['idcontenido'];
    $nombreArea=$_POST['nombreArea'];
    $data['nombreArea']=$nombreArea;
    $this->estudiantes_model->eliminarC($idcontenido);
    $this->load->view('head');
    $this->load->view('eliminarCmensaje',$data);
    //$this->load->view('footer');
  }
   public function evaluaciones()//cargar inicio
  {
      //  $data['estudiantes']=$this->estudiantes_model->retornarEstudiantes();

        $this->load->view('head');
        $this->load->view('vistaEvaluacion');
  }

  public function chat()//cargar inicio
  {
      //  $data['estudiantes']=$this->estudiantes_model->retornarEstudiantes();

        $this->load->view('head');
        $this->load->view('vistaChat');



  }
}
