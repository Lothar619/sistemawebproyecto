<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class estudiantes_model extends CI_Model {

	public function  retornarAu()
	{
        $this->db->select('*');
        $this->db->from('perfilusuario');
       // $this->db->order_by('idestudiante');
        return $this->db->get();

	
    }
  public function agregarU($ps)
  {
    $this->db->insert('perfilusuario',$ps);
    
  }

  public function recuperarU($idperfilUsuario)
  {
    $this->db->select('*');
    $this->db->from('perfilusuario');
    $this->db->where('idperfilUsuario',$idperfilUsuario);
    return $this->db->get();
  }

  public function modificarU($idperfilUsuario,$ps)
  {
    $this->db->where('idperfilUsuario',$idperfilUsuario);
    $this->db->update('perfilusuario',$ps);
  }

  public function eliminarU($idperfilUsuario)
  {
    $this->db->where('idperfilUsuario',$idperfilUsuario);
    $this->db->delete('perfilUsuario');
    
  }

  public function  retornarCo()
  {
        $this->db->select('*');
        $this->db->from('contenido');
       // $this->db->order_by('idestudiante');
        return $this->db->get();
  
    }
      public function agregarC($c)
  {
    $this->db->insert('contenido',$c);
    
  }
  public function recuperarC($idcontenido)
  {
    $this->db->select('*');
    $this->db->from('contenido');
    $this->db->where('idcontenido',$idcontenido);
    return $this->db->get();
  }

  public function modificarC($idcontenido,$c)
  {
    $this->db->where('idcontenido',$idcontenido);
    $this->db->update('contenido',$c);
  }
public function eliminarC($idcontenido)
  {
    $this->db->where('idcontenido',$idcontenido);
    $this->db->delete('contenido');
    
  }

}