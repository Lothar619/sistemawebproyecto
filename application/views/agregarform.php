<div class="container">
    
<?php echo form_open_multipart('estudiantes/agregardb'); ?>

<p><input type="text" name="nombres"  required pattern="[A-Za-z]+" title="solo letras" placeholder="Ingrese nombre"></input></p>
<p><input type="text" name="primerApellido"  required pattern="[A-Za-z]+" title="solo letras" placeholder="Ingrese primer apellido"></input></p>
<p><input type="text" name="segundoApellido" pattern="[A-Za-z]+" title="solo letras" placeholder="Ingrese segundo apellido"></input></p>
<p><input type="text" name="login"  required maxlength="6" pattern="[A-Za-z]+" title="solo letras" placeholder="Ingrese su login"></input></p>
<p><input type="password" name="password"  required maxlength="6" title="solo 6 caracteres" placeholder="Ingrese contraseña"></input></p>
<p><input type="text" name="nombreNiño"  required pattern="[A-Za-z]+" title="solo letras" placeholder="Ingrese nombre del niño"></input></p>

<p><input type="text" name="cursoNiño" placeholder="Ingrese curso del niño" list="l1" ><datalist id="l1">
        <option>kinder</option>
        <option>1ro Primaria</option>
        <option>2do Primaria</option>
        <option>3ro Primaria</option>
        <option>4to Primaria</option>
        <option>5to Primaria</option>
        <option>6to Primaria</option>
    </datalist></input></p>

<p><input type="text" name="ocupacion"  required pattern="[A-Za-z]+" title="solo letras" placeholder="Ingrese ocupacion"></input></p>


<button type="submit" class="btn btn-primary">Agregar</button>
<?php echo form_close(); ?>
      
 </div>

         

