<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>


<link rel="stylesheet" href="<?=base_url()?>/bootstrap/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="<?=base_url()?>/bootstrap/js/jquery-3.3.1.js"></script>

<!-- Popper JS -->
<script src="<?=base_url()?>/bootstrap/js/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="<?=base_url()?>/bootstrap/js/bootstrap.min.js"></script>
	
</head>
  
<body>
  <nav class="navbar navbar-expand-lg bg-primary navbar-dark">
 <!-- <nav class="navbar navbar-expand-lg bg-primary navbar-dark fixed-top"> -->
  <!-- <nav class="navbar navbar-expand-sm bg-dark navbar-dark"> -->
  <!-- <nav class="navbar navbar-expand-sm bg-light navbar-light"> -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <img src="<?php echo base_url(); ?>img/logo2.png" height="40" width="40" />
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>index.php/estudiantes/index2">INICIO</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>index.php/estudiantes/Notas">REGISTRO</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>index.php/estudiantes/contenido">CONTENIDO</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>index.php/estudiantes/evaluaciones">EVALUACIONES</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url()?>index.php/estudiantes/chat">CHAT</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#"></a>
      </li>
    </ul>
  </nav>

  <br>
  <br>