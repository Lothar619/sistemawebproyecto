<div class="container">   
   
  <div align="center">
<img src="<?php echo base_url(); ?>img/fondo2.jpg" height="200" width="1200" />

</div>
<section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">AUTISMO</h2>
            
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
             
             
           </span>
            <h4 class="service-heading">Historia</h4>
            <p class="text-muted">La palabra autismo fue utilizada por primera vez en 1912 por el psiquiatra suizo Eugene Bleuler, en un artículo publicado en el American Journal of Insanity, para referirse a uno de los síntomas de la esquizofrenia o Dementia praecox, un trastorno de muy rara ocurrencia en la infancia.25​ Lo construyó a partir del griego autos que significa "uno mismo".En 1938, el médico austríaco Hans Asperger utilizó la terminología de Bleuler usando "autístico" en el sentido moderno para describir en psicología infantil a niños que no compartían con sus pares, no comprendían los términos «cortesía» o «respeto» y presentaban además hábitos y movimientos estereotipados. Denominó el cuadro «psicopatía autística».En 1944 elaboraría un artículo para ahondar en este síndrome; sin embargo, sus trabajos pasaron desapercibidos durante décadas ante la comunidad científica internacional debido a las circunstancias históricas posteriores, ya que publicó en alemán.</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
          
         

           </span>
            <h4 class="service-heading">Causas</h4>
            <p class="text-muted">Los científicos no conocen las causas exactas del autismo. El autismo se describió por primera vez en la década de 1940, pero se sabía muy poco del mismo hasta las últimas décadas. Incluso hoy en día, hay mucho que no sabemos sobre el autismo.Dado que este trastorno es tan complejo y que no hay dos personas con autismo exactamente iguales, es probable que haya muchas causas del autismo. Hay abundante evidencia que sustenta la idea de que los genes son una de las principales causas del ASD. Más de 100 genes en diferentes cromosomas podrían estar involucrados en el desarrollo del ASD en diferentes grados. Los investigadores también estudian otros factores biológicos, además de los genes, que podrían estar involucrados en el ASD. Algunos de estos factores incluyen:

            problemas con las conexiones cerebrales;
            problemas con el crecimiento o crecimiento excesivo de ciertas zonas del cerebro;
            problemas con el metabolismo (el sistema de producción de energía del cuerpo);
            problemas con el sistema inmunitario del organismo, que protege de las infecciones.</p>

          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              
              
              
            </span>
            <h4 class="service-heading">Caracteristicas</h4>
            <p class="text-muted">• Deficiencias persistentes en la comunicación y en la interacción social.
• Patrones restrictivos y repetitivos de comportamiento, intereses o actividades.• En el parvulario y en la escuela, hay falta de interés por los otros niños.
• No comparten intereses (no acostumbran a señalar con el dedo aquello que les llama la atención para compartirlo con los demás).
• Ausencia de juego simbólico (dar de comer a muñecas, hacer cocinitas, jugar a coches como si fueran de verdad, etc.).
• Se establece poco contacto visual y no observan la expresión de la cara del interlocutor cuando juntos están viendo alguna cosa inusual. No acostumbran a realizar la sonrisa social.
• Su lenguaje, si existe, es literal (no entienden las bromas, los chistes, los dobles sentidos ni las metáforas).
• Evitan el contacto físico o les gusta más bien poco.</p>
          </div>
        </div>
      </div>
      <div id="imagen">
</div>
</div>