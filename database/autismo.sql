-- MySQL Script generated by MySQL Workbench
-- 05/31/19 10:34:23
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema autismo
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema autismo
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `autismo` DEFAULT CHARACTER SET utf8 ;
USE `autismo` ;

-- -----------------------------------------------------
-- Table `autismo`.`rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autismo`.`rol` (
  `idRol` INT(11) NOT NULL,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idRol`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autismo`.`perfilusuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autismo`.`perfilusuario` (
  `idperfilUsuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nombres` VARCHAR(45) NOT NULL,
  `primerApellido` VARCHAR(45) NOT NULL,
  `segundoApellido` VARCHAR(45) NULL DEFAULT NULL,
  `ocupacion` VARCHAR(45) NULL DEFAULT NULL,
  `nombreNiño` VARCHAR(45) NULL DEFAULT NULL,
  `cursoNiño` VARCHAR(45) NULL DEFAULT NULL,
  `fechaRegistro` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaActualizacion` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `Rol_idRol` INT(11) NOT NULL,
  PRIMARY KEY (`idperfilUsuario`, `Rol_idRol`),
  INDEX `fk_perfilUsuario_Rol1_idx` (`Rol_idRol` ASC),
  CONSTRAINT `fk_perfilUsuario_Rol1`
    FOREIGN KEY (`Rol_idRol`)
    REFERENCES `autismo`.`rol` (`idRol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autismo`.`contenido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autismo`.`contenido` (
  `idcontenido` INT(11) NOT NULL AUTO_INCREMENT,
  `nombreArea` VARCHAR(50) NULL DEFAULT NULL,
  `descripcion` VARCHAR(150) NULL DEFAULT NULL,
  `fechResgisto` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `perfilUsuario_idperfilUsuario` INT(11) NOT NULL,
  PRIMARY KEY (`idcontenido`, `perfilUsuario_idperfilUsuario`),
  INDEX `fk_contenido_perfilUsuario1_idx` (`perfilUsuario_idperfilUsuario` ASC),
  CONSTRAINT `fk_contenido_perfilUsuario1`
    FOREIGN KEY (`perfilUsuario_idperfilUsuario`)
    REFERENCES `autismo`.`perfilusuario` (`idperfilUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autismo`.`evaluacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autismo`.`evaluacion` (
  `idEvaluacion` INT(11) NOT NULL,
  `prueba` VARCHAR(70) NULL DEFAULT NULL,
  `descripcion` VARCHAR(150) NULL DEFAULT NULL,
  `contenido_idcontenido` INT(11) NOT NULL,
  PRIMARY KEY (`idEvaluacion`, `contenido_idcontenido`),
  INDEX `fk_Evaluacion_contenido1_idx` (`contenido_idcontenido` ASC),
  CONSTRAINT `fk_Evaluacion_contenido1`
    FOREIGN KEY (`contenido_idcontenido`)
    REFERENCES `autismo`.`contenido` (`idcontenido`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autismo`.`reporteevaluacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autismo`.`reporteevaluacion` (
  `idreporteEvaluacion` INT(11) NOT NULL,
  `puntaje` INT(11) NULL DEFAULT NULL,
  `perfilUsuario_idperfilUsuario` INT(11) NOT NULL,
  `Evaluacion_idEvaluacion` INT(11) NOT NULL,
  PRIMARY KEY (`idreporteEvaluacion`, `perfilUsuario_idperfilUsuario`, `Evaluacion_idEvaluacion`),
  INDEX `fk_reporteEvaluacion_perfilUsuario1_idx` (`perfilUsuario_idperfilUsuario` ASC),
  INDEX `fk_reporteEvaluacion_Evaluacion1_idx` (`Evaluacion_idEvaluacion` ASC),
  CONSTRAINT `fk_reporteEvaluacion_Evaluacion1`
    FOREIGN KEY (`Evaluacion_idEvaluacion`)
    REFERENCES `autismo`.`evaluacion` (`idEvaluacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporteEvaluacion_perfilUsuario1`
    FOREIGN KEY (`perfilUsuario_idperfilUsuario`)
    REFERENCES `autismo`.`perfilusuario` (`idperfilUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
